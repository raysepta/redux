import { StatusBar } from 'expo-status-bar';
import { useState } from 'react';
import { StyleSheet } from 'react-native';
import { Provider } from 'react-redux';

import store from './redux/store';
import Home from './screens/Home';



export default function App() {
  return (
    <Provider store={store}>
      <Home />
    </Provider>
  );
}


