import { StyleSheet } from "react-native"

const vehiclesStyles = StyleSheet.create({
  containerProfile: {
    flex: 1,
    backgroundColor: '#fff',
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'center',
    marginBottom: 20,

  },
  containerBalance: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    backgroundColor: '#F2DEBA',
    position: 'absolute',
    width: 162,
    height: 73,
    left: 7,
    top: 57,
    borderRadius: 10,

  },
  profileImage: {
    width: 40,
    height: 40,
    marginTop: 15,
    marginLeft: 20,
    alignSelf: 'flex-start',
  },
  titleProfile: {
    position: 'absolute',
    width: 225,
    height: 50,
    left: 153,
    top: 65,
    backgroundColor: '#3A8891',
    borderRadius: 10,
  },
  profile: {
    flex: 1,
    flexDirection: 'column',
    textAlign: 'left',
    alignSelf: 'flex-start',
    marginTop: 15,
  },
  profileFont: {
    fontStyle: 'normal',

    fontSize: 14,
    textAlign: 'center',

    color: '#0E5E6F',
  },
  profileTitleFont: {
    fontStyle: 'normal',

    fontSize: 14,
    textAlign: 'left',
    paddingLeft: 20,
    paddingTop: 16,


    color: '#F2DEBA',
  },




  row: {
    flex: 1,
    justifyContent: 'space-around',
  },
  vehiclesFont: {
    fontStyle: 'normal',
    fontSize: 20,
    color: '#0E5E6F'
  },
  priceFont: {
    fontStyle: 'normal',
    fontSize: 16,
    color: '#0E5E6F'
  },
  image: {
    width: 300,
    height: 120,
    borderRadius: 10,
  },
  rentButton: {
    width: 79,
    height: 33,

    backgroundColor: '#F2DEBA',
    borderRadius: 6,
  },
  returnButton: {
    width: 79,
    height: 33,


    backgroundColor: '#3A8891',
    borderRadius: 6,
  },
  disableButton: {
    width: 79,
    height: 33,

    borderWidth: 2,

    backgroundColor: '#EB6440',
    borderRadius: 6,
  },


  rentFontButton: {
    fontStyle: 'normal',
    textAlign: 'center',
    alignItems: 'center',
    alignContent: 'center',
    fontSize: 16,
    color: '#0E5E6F'
  },
  returnFontButton: {
    fontStyle: 'normal',
    textAlign: 'center',
    alignItems: 'center',
    alignContent: 'center',
    fontSize: 16,
    color: '#F2DEBA',
  },
  disableFontButton: {
    fontStyle: 'normal',
    textAlign: 'center',
    alignItems: 'center',
    alignContent: 'center',
    fontSize: 16,
    color: '#F2DEBA',
  },

})

export default vehiclesStyles