import { combineReducers } from 'redux'
import statusReducer from './statusReducer';
import userProfileReducer from './userProfileReducer';

//install redux thunk
const reducers = combineReducers({
   status: statusReducer,
   userProfile: userProfileReducer
})

export default reducers