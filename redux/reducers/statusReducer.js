const statusReducer = (state = [], action) => {
    switch (action.type) {
        case 'FETCH_DATA':
            console.log(action.payload)
            return action.payload;
            break;
        case 'RENT':
            return state.map(car => {
                if (car.id === action.payload) {
                    car.status = true
                }
                console.log(car)
                return car;
            })
            break;
        case 'RETURN':
            return state.map(car => {
                if (car.id === action.payload) {
                    car.status = false
                }
                console.log(car)
                return car;
            })
            break;
        default:
            return state;
            break;
    }
}

export default statusReducer