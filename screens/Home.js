import { View, Text, SafeAreaView, Image } from 'react-native'
import React from 'react'
import { useSelector } from 'react-redux'
import vehiclesStyle from '../components/vehiclesStyle'
import VehiclesScreen from './VehiclesScreen'
import vehiclesStyles from '../components/vehiclesStyle'



export default function Home() {
  const profile = useSelector((state) => state.userProfile)

  return (
    <SafeAreaView style={vehiclesStyles.containerProfile}>

      <View style={vehiclesStyle.titleProfile} >
        <Text style={vehiclesStyle.profileTitleFont} >Ready to going on adventure?</Text>
      </View>
      <View style={vehiclesStyle.containerBalance} >
        <Image style={vehiclesStyle.profileImage} source={require('/Users/raysepta/Documents/Development/G2Academy/Materi/Visual Studio Code/React JS/Day21/Redux/assets/profile.png')} />
        <View style={vehiclesStyle.profile} >
          <Text style={vehiclesStyle.profileFont} >{profile.user} </Text>
          <Text style={vehiclesStyle.profileFont} >{profile.balance.toLocaleString("en-US", { style: "currency", currency: "USD" })}</Text>
        </View>
      </View>
      <VehiclesScreen />
    </SafeAreaView>
  )
}
