import { View, Text, FlatList, Alert, TouchableOpacity, Image, StyleSheet } from 'react-native';
import React, { useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import vehiclesStyle from '../components/vehiclesStyle';

export default function VehiclesScreen() {
  const cars = useSelector((state) => state.status);
  const profile = useSelector((state) => state.userProfile)

  const dispatch = useDispatch();

  const fetchVechiles = async () => {
    try {
      let response = await fetch(`http://movie-app-g2.herokuapp.com/vehicles`);
      if (response.ok) {
        let data = await response.json();
        dispatch({ type: 'FETCH_DATA', payload: data })
      }
    } catch (err) {
      Alert.alert("Error Fetch Vehicles")
    }
  }

  const editRentVehicles = async (id) => {
    try {
      let response = await fetch(`http://movie-app-g2.herokuapp.com/vehicles/${id}`, {
        method: 'PATCH',
        body: JSON.stringify({
          status: true

        }),
        headers: {
          'Content-type': 'application/json; charset=UTF-8',
        },
      });
      if (response.ok) {
        let dataUpdate = await response.json();
        dispatch({ type: 'RENT', payload: id });
        dispatch({ type: 'USER_BALANCE', payload: dataUpdate.price })

      }
    } catch (err) {
      Alert.alert("Error Rent Vehicles")
    }
  }

  const editReturnVehicles = async (id) => {
    try {
      let response = await fetch(`http://movie-app-g2.herokuapp.com/vehicles/${id}`, {
        method: 'PATCH',
        body: JSON.stringify({
          status: false

        }),
        headers: {
          'Content-type': 'application/json; charset=UTF-8',
        },
      });
      if (response.ok) {
        let dataUpdate = await response.json();
        dispatch({ type: 'RETURN', payload: id });

      }
    } catch (err) {
      Alert.alert("Error Return Vehicles")
    }
  }




  useEffect(() => {
    fetchVechiles()
  }, [])

  return (
    <View>
      <FlatList
        style={{ marginTop: 100, marginBottom: 20 }}
        data={cars}
        renderItem={({ item }) => (
          <View >
            <View>
              <Image
                style={vehiclesStyle.image}
                source={{ uri: `${item.image}` }}
              />
            </View>
            <View>
              <Text style={vehiclesStyle.vehiclesFont} >{item.type_of_vehicle}</Text>
              <Text style={vehiclesStyle.priceFont} >{item.price.toLocaleString("en-US", { style: "currency", currency: "USD" })}</Text>
            </View>
            {
              item.status  ?
              <ReturnButton
              onPress={() => {
                editReturnVehicles(item._id)
              }}

            />
               :
               
                <RentButton
                onPress={() => {
                  editRentVehicles(item._id)
                  
                }}

              /> 
            }
            {
              item.price > profile.balance ?
              <DisableButton /> 
              :
              null
            }
            
              
            

          </View>

        )}
        keyExtractor={(item) => item._id}
      />

    </View>
  )
}
const RentButton = (props) => {
  return (
    <View >
      <TouchableOpacity style={vehiclesStyle.rentButton}
        onPress={props.onPress}
      >
        <Text style={vehiclesStyle.rentFontButton}>Rent</Text>
      </TouchableOpacity>

    </View>
  )
}

const ReturnButton = (props) => {
  return (
    <View >
      <TouchableOpacity style={vehiclesStyle.returnButton}

        onPress={props.onPress}
      >
        <Text style={vehiclesStyle.returnFontButton}>Return</Text>
      </TouchableOpacity>
    </View>
  )
}

const DisableButton = () => {
  return (
    <View >
      <TouchableOpacity style={vehiclesStyle.disableButton}

      >
        <Text style={vehiclesStyle.disableFontButton}>Rent</Text>
      </TouchableOpacity>
    </View>
  )
}

